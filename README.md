### I. Prerequisites
- docker 20.10+
- docker-compose 1.29+
- java 11+
- maven 3.6+
- ram 2GiB+
- excellent read & write performance (as they said, for Elasticsearch and DB)

### II. Preparation

##### 1. Update some kernel parameters
```bash
cat <<EOF | sudo tee /etc/sysctl.d/99-sonarqube.conf
vm.max_map_count=262144
fs.file-max=65536
EOF
```
Reload and verify:
```bash
sudo service procps force-reload
service procps status
sysctl vm.max_map_count
sysctl fs.file-max
```

##### 2. Increase some user limits
```bash
cat <<EOF | sudo tee /etc/security/limits.d/99-sonarqube.conf
*   -   nofile   131072
*   -   nproc    8192
EOF
```
Re-login and verify:
```bash
ulimit -n
ulimit -u
```

### III. Deployment

##### 1. Bring them up, as always
```bash
docker-compose up -d && docker-compose logs -f
```

##### 2. Check it out:
Once your instance is up and running, Log in to http://127.0.0.1:9000 using System Administrator credentials:
- login: admin
- password: admin

Then follow the getting started guide!

### IV. Some good link to follow up
Here we use `8.9-community version`, as a LTS version.

- https://docs.sonarqube.org/8.9/requirements/requirements/
- https://docs.sonarqube.org/8.9/setup/install-cluster/
- https://docs.sonarqube.org/8.9/setup/operate-cluster/#header-11 (Cluster/DC limitations)
- https://docs.sonarqube.org/8.9/setup/environment-variables/
- https://docs.sonarqube.org/8.9/analysis/scan/sonarscanner-for-maven/
- https://docs.sonarqube.org/8.9/analysis/scan/sonarscanner-for-jenkins/
- https://docs.sonarqube.org/8.9/analysis/jenkins/
- https://docs.sonarqube.org/8.9/analysis/scm-integration/
- https://docs.sonarqube.org/8.9/analysis/gitlab-integration/
- https://docs.sonarqube.org/8.9/user-guide/concepts/

### V. TODO
- Gitlab
- Jenkins
- LDAP
- SSO
- Keycloak
